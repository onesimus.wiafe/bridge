package abstractions;

import implementations.Pizza;

public class ItalianRestaurant extends Restaurant {

    public ItalianRestaurant(Pizza pizza){
        super(pizza);
    }
    @Override
    void addSauce() {
        pizza.setSauce("Special Italian sauce");

    }

    @Override
    void addToppings() {
        pizza.setToppings("Broccoli, Potato, Olives, Radicchio, Capsicum, Eggplant");
    }

    @Override
    void makeCrust() {
        pizza.setCrust("Thin");
    }
}
