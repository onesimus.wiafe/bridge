package abstractions;

import implementations.Pizza;

public abstract class Restaurant {
    protected Pizza pizza;

    public Restaurant(Pizza pizza){
        this.pizza = pizza;
    }

    abstract void addSauce();
    abstract void addToppings();
    abstract void makeCrust();

    public void prepare() {
        System.out.println("-------------Preparation-------------");
        System.out.println("Starting the Preparation...");
        makeCrust();
        addSauce();
        addToppings();
        pizza.assemble();
        System.out.println("Order in the oven!\nWe're working on delivery!");
        System.out.println("-----------------------------------\n");
    }
}
