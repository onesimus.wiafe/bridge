package abstractions;

import implementations.Pizza;

public class GhanaianRestaurant extends Restaurant {

    public GhanaianRestaurant(Pizza pizza){
        super(pizza);
    }
    @Override
    void addSauce() {
        pizza.setSauce("Organic generic sauce");

    }

    @Override
    void addToppings() {
        pizza.setToppings("Red Pepper, Yellow Pepper, Tomato, Onion");
    }

    @Override
    void makeCrust() {
        pizza.setCrust("Extra thick");
    }
}
