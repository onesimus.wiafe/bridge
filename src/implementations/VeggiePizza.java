package implementations;

public class VeggiePizza extends Pizza{
    @Override
    public void assemble() {
        System.out.println("Adding " + sauce);
        System.out.println("Lining with "+ toppings);
        System.out.println("Setting our crust to "+ crust);
        System.out.println("Adding mushrooms");
        System.out.println("Sprinkling Almond cheese");
    }
}
