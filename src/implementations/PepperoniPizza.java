package implementations;

public class PepperoniPizza extends Pizza {

    @Override
    public void assemble() {
        System.out.println("Adding " + sauce);
        System.out.println("Topping with "+ toppings);
        System.out.println("Setting our crust to "+ crust);
        System.out.println("Topping up with Pepperoni");
        System.out.println("Sprinkling Mozzarella Cheese");
    }


}
