import abstractions.GhanaianRestaurant;
import abstractions.ItalianRestaurant;
import abstractions.Restaurant;
import implementations.PepperoniPizza;
import implementations.Pizza;
import implementations.VeggiePizza;

public class Main {
    public static void main(String[] args) {

        Pizza pepperoniPizza = new PepperoniPizza();
        Pizza veggiePizza = new VeggiePizza();

        Restaurant eddiesPepperoni = new GhanaianRestaurant(pepperoniPizza);
        eddiesPepperoni.prepare();
        Restaurant guilliePepperoni = new ItalianRestaurant(pepperoniPizza);
        guilliePepperoni.prepare();

//        Restaurant papasVeggie = new GhanaianRestaurant(veggiePizza);
//        Restaurant guillieVeggie = new GhanaianRestaurant(veggiePizza);
//
//        guillieVeggie.prepare();
//        papasVeggie.prepare();
    }
}